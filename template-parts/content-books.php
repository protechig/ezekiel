<?php
/**
 * Template part for displaying books.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ezekiel
 */

?>

<article <?php post_class(); ?>>
    <?php if( get_field('book_cover') ): ?>
    <div class="book-cover">
        <a href="<?php  the_permalink(); ?>" target="_blank" class="book-header">
            <img src="<?php the_field('book_cover'); ?>" alt="<?php the_field('book_cover_alt_text'); ?>" class="book-cover-image">
        </a>
    </div>
    <?php endif; ?>

    <div class="book-meta">
        <span class="book-release-date">Released – <?php the_field('book_release_date'); ?></span>

        <a href="<?php  the_permalink(); ?>" target="_blank" class="book-header">
            <?php the_title( '<h2 class="book-title">', '</h2>'); ?>
        </a>

        <span class="book-excerpt">
            <?php echo book_archive_excerpt(); ?> <a href="<?php the_permalink(); ?>" target="_blank" class="book-read-more">read full excerpt »</a>
        </span>

        <a href="<?php the_field('book_link'); ?>" target="_blank" class="book-link">Purchase <span class="book-format"><?php the_field('book_format'); ?></span></a>
    </div>
</article>