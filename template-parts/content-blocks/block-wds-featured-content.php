<?php
/**
 * The template used for displaying a Featured Content block.
 *
 * @package Ezekiel
 */

// Set up fields.

$alignment               = ez_get_block_alignment( $block );
$classes                 = ez_get_block_classes( $block );
$book                    = get_field( 'book' );
$view_all_link_1         = get_field( 'view_all_link_2' );
$view_all_link_text_1    = get_field( 'view_all_link_text_1' );
$featured_piece_one      = get_field( 'featured_piece_one' ); 
$featured_piece_two      = get_field( 'featured_piece_two' ); 
$view_all_link_2         = get_field( 'view_all_link_2' );
$view_all_link_text_2    = get_field( 'view_all_link_text_2' );
// Start a <container> with possible block options.
ez_display_block_options(
	array(
		'block'     => $block,
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block featured-content-block container' . esc_attr( $alignment . $classes ), // Container class.
	)
);

?>

    <div class="featured-content-block-contentbook-meta display-flex">
		<div class="left-two-thirds">
			<h2 class="accent">Books In Print</h2>
			<div class="books hentry">
				<div class="book-cover">
					<a href="<?php the_field( 'book_link', $book ); ?>" target="_blank">
						<img src="<?php the_field( 'book_cover', $book ); ?>" alt="<?php the_field( 'book_cover_alt_text', $book ); ?>">
					</a>
				</div>

				<div class="book-meta">
					<span class="book-release-date">Release Date – <?php the_field( 'book_release_date', $book ); ?></span>
					
					<a href="http://ezekiel.test/books/brothers-emanuel-a-memoir-of-an-american-family/" target="_blank" class="book-header">
						<h2 class="book-title"><?php the_field( 'book_title', $book ); ?></h2>
					</a>

					<span class="book-excerpt">
						<?php echo custom_field_excerpt(); ?>
						
						<a href="http://ezekiel.test/books/brothers-emanuel-a-memoir-of-an-american-family/" target="_blank" class="book-read-more">read full excerpt »</a>
					</span>

					<a href="<?php the_field('book_link', $book); ?>" target="_blank" class="book-link">Purchase <span class="book-format"><?php the_field('book_format', $book); ?></span></a>
				</div>
			</div>
			<div class="view-all">
				<a href="<?php echo esc_url( $view_all_link_1 ); ?>" class="view-all-link"><?php echo esc_html( $view_all_link_text_1 ); ?></a>
			</div>
		</div>

		<div class="right-third">
			<h2 class="accent">Featured Writing</h2>

			<article class="external-article">
				<span class="external-article-date"><?php the_field( 'external_article_date', $featured_piece_one ); ?></span>
				<h2 class="external-article-title">
					<a href="<?php the_field( 'external_article_link', $featured_piece_one ); ?>" class="external-article-link">
						<?php the_field( 'external_article_title', $featured_piece_one ); ?>
					</a>
				</h2>
			</article>

			<article class="external-article">
				<span class="external-article-date"><?php the_field( 'external_article_date', $featured_piece_two ); ?></span>
				<h2 class="external-article-title">
					<a href="<?php the_field( 'external_article_link', $featured_piece_two ); ?>" class="external-article-link">
						<?php the_field( 'external_article_title', $featured_piece_two ); ?>
					</a>
				</h2>
			</article>

			<div class="view-all">
				<a href="<?php echo esc_url( $view_all_link_2 ); ?>" class="view-all-link"><?php echo esc_html( $view_all_link_text_2 ); ?></a>
			</div>
		</div>
    </div>
</section>
