<?php
/**
 * The template used for displaying a Image and Quote block.
 *
 * @package Ezekiel
 */

// Set up fields.
$image       = get_field('image');
$quote       = get_field( 'quote' );
$alignment   = ez_get_block_alignment( $block );
$classes     = ez_get_block_classes( $block );

// Start a <container> with possible block options.
ez_display_block_options(
	array(
		'block'     => $block,
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block image-and-quote-block' . esc_attr( $alignment . $classes ), // Container class.
	)
);
?>
	<div class="container display-flex align-center">
        <div class="left-two-thirds">
            <img src="<?php echo esc_url( $image['url'] ); ?>" alt="<?php echo esc_attr( $image['alt'] ); ?>">
        </div>
        
        <div class="right-third">
            <?php echo ez_get_the_content( $quote ); ?>
        </div>
    </div>
</section>
