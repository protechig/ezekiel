<?php
/**
 * The template used for displaying a Featured Content block.
 *
 * @package Ezekiel
 */

// Set up fields.

$alignment               = ez_get_block_alignment( $block );
$classes                 = ez_get_block_classes( $block );

$recent_appearance_one                    = get_field( 'recent_appearance_one' );
$recent_appearance_two                    = get_field( 'recent_appearance_two' );
$recent_appearance_three                   = get_field( 'recent_appearance_three' );

$recent_opinion_one                   = get_field( 'recent_opinion_one' );
$recent_opinion_two                   = get_field( 'recent_opinion_two' );
$recent_opinion_three                   = get_field( 'recent_opinion_three' );

$recent_academic_writing_one                   = get_field( 'recent_academic_writing_one' );
$recent_academic_writing_two                   = get_field( 'recent_academic_writing_two' );
$recent_academic_writing_three                   = get_field( 'recent_academic_writing_three' );

// Start a <container> with possible block options.
ez_display_block_options(
	array(
		'block'     => $block,
		'container' => 'section', // Any HTML5 container: section, div, etc...
		'class'     => 'content-block recent-content-block container' . esc_attr( $alignment . $classes ), // Container class.
	)
);

?>

    <div class="recent-content-block-contentbook-meta display-flex">
        <div class="third">
            <h2 class="accent">Recent Media Appearances</h2>

            <article class="<?php the_field( 'appearance_state', $recent_appearance_one ); ?> appearance appearances" target="_blank">
                <span class="appearance-date"><?php the_field( 'appearance_date', $recent_appearance_one ); ?></span>

                <a href="<?php the_field( 'appearance_link', $recent_appearance_one ); ?>" class="appearances-header external-article-link">
                    <h2 class="appearance-title recent-content-block"><?php the_field( 'appearance_title', $recent_appearance_one ); ?></h2>
                </a>
            </article>

            <article class="<?php the_field( 'appearance_state', $recent_appearance_two ); ?> appearance appearances" target="_blank">
                <span class="appearance-date"><?php the_field( 'appearance_date', $recent_appearance_two ); ?></span>

                <a href="<?php the_field( 'appearance_link', $recent_appearance_two ); ?>" class="appearances-header external-article-link">
                    <h2 class="appearance-title recent-content-block"><?php the_field( 'appearance_title', $recent_appearance_two ); ?></h2>
                </a>
            </article>

            <article class="<?php the_field( 'appearance_state', $recent_appearance_three ); ?> appearance appearances" target="_blank">
                <span class="appearance-date"><?php the_field( 'appearance_date', $recent_appearance_three ); ?></span>

                <a href="<?php the_field( 'appearance_link', $recent_appearance_three ); ?>" class="appearances-header external-article-link">
                    <h2 class="appearance-title recent-content-block"><?php the_field( 'appearance_title', $recent_appearance_three ); ?></h2>
                </a>
            </article>

            <div class="view-all">
                <a href="https://ezekiel.protechig.com/appearances/" class="view-all-link" target="_blank">View All Appearances</a>
            </div>
        </div>

        <div class="third">
            <h2 class="accent">Opinions</h2>

            <article <?php post_class( 'external_article_type external-article opinion', $recent_opinion_one ); ?>>
                <span class="external-article-date"><?php the_field( 'external_article_date', $recent_opinion_one ); ?></span>
                
                <a href="<?php the_field( 'external_article_link', $recent_opinion_one ); ?>" class="external-article-link" target="_blank">
                    <h2 class="external-article-title recent-content-block"><?php the_field( 'external_article_title', $recent_opinion_one ); ?></h2>
                </a>
            </article>

            <article <?php post_class( 'external_article_type external-article opinion', $recent_opinion_two ); ?>>
                <span class="external-article-date"><?php the_field( 'external_article_date', $recent_opinion_two ); ?></span>

                <a href="<?php the_field( 'external_article_link', $recent_opinion_two ); ?>" class="external-article-link" target="_blank">
                    <h2 class="external-article-title recent-content-block"><?php the_field( 'external_article_title', $recent_opinion_two ); ?></h2>
                </a>
            </article>

            <article <?php post_class( 'external_article_type external-article opinion', $recent_opinion_three ); ?>>
                <span class="external-article-date"><?php the_field( 'external_article_date', $recent_opinion_three ); ?></span>

                <a href="<?php the_field( 'external_article_link', $recent_opinion_three ); ?>" class="external-article-link" target="_blank">
                    <h2 class="external-article-title recent-content-block"><?php the_field( 'external_article_title', $recent_opinion_three ); ?></h2>
                </a>
            </article>

            <div class="view-all">
                <a href="https://ezekiel.protechig.com/category/popular-writing/" class="view-all-link" target="_blank">View All Opinions</a>
            </div>
        </div>

        <div class="third">
            <h2 class="accent">Academic Writing</h2>

            <article <?php post_class( 'external_article_type external-article academic-writing', $recent_academic_writing_one ); ?>>
                <span class="external-article-date"><?php the_field( 'external_article_date', $recent_academic_writing_one ); ?></span>

                <a href="<?php the_field( 'external_article_link', $recent_academic_writing_one ); ?>" class="external-article-link" target="_blank">
                    <h2 class="external-article-title recent-content-block"><?php the_field( 'external_article_title', $recent_academic_writing_one ); ?></h2>
                </a>
            </article>

            <article <?php post_class( 'external_article_type external-article academic-writing', $recent_academic_writing_two ); ?>>
                <span class="external-article-date"><?php the_field( 'external_article_date', $recent_academic_writing_two ); ?></span>

                <a href="<?php the_field( 'external_article_link', $recent_academic_writing_two ); ?>" class="external-article-link" target="_blank">
                    <h2 class="external-article-title recent-content-block"><?php the_field( 'external_article_title', $recent_academic_writing_two ); ?></h2>
                </a>
            </article>

            <article <?php post_class( 'external_article_type external-article academic-writing', $recent_academic_writing_three ); ?>>
                <span class="external-article-date"><?php the_field( 'external_article_date', $recent_academic_writing_three ); ?></span>

                <a href="<?php the_field( 'external_article_link', $recent_academic_writing_three ); ?>" class="external-article-link" target="_blank">
                    <h2 class="external-article-title recent-content-block"><?php the_field( 'external_article_title', $recent_academic_writing_three ); ?></h2>
                </a>
            </article>

            <div class="view-all">
                <a href="https://ezekiel.protechig.com/category/academic-writing/" class="view-all-link" target="_blank">View All Academic Writing</a>
            </div>
        </div>
    </div>
</section>
