<?php
/**
 * Template part for displaying external articles.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ezekiel
 */

$external_article_type = get_field('external_article_type');

?>

<article <?php post_class( 'external-article', $external_article_type = ''); ?>>
    <span class="external-article-date"><?php the_field('external_article_date'); ?></span>
        <a href="<?php the_field('external_article_link'); ?>" class="external-article-link" target="_blank">
            <?php the_title( '<h2 class="external-article-title">', '</h2>'); ?>
        </a>

    <p>
        <span class="external-article-author">Written by <?php the_field('external_article_author'); ?> for </span>
        <a href="<?php the_field('external_article_link'); ?>" class="external-article-link" target="_blank"><span class="external-article-outlet"><?php the_field('external_article_outlet'); ?></span> –– Read Now</a>
    </p>
</article>
