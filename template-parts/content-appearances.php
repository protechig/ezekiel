<?php
/**
 * Template part for displaying appearances.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ezekiel
 */

$appearance_state = get_field('appearance_state');

?>

<article <?php post_class($appearance_state); ?>>
    <span class="appearance-date"><?php the_field('appearance_date'); ?></span>
    <a href="<?php  the_permalink(); ?>" target="_blank" class="appearances-header">
        <?php the_title( '<h2 class="appearances-title">', '</h2>'); ?>
    </a>
    <p class="appearances-content"><a href="<?php the_field('appearance_link'); ?>" target="_blank">Watch the segment here</a></p>
</article>