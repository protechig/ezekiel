<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Ezekiel
 */

?>

	<footer class="site-footer">
		<div class="footer-widgets">
			<div class="container">
				<?php if ( is_active_sidebar( 'footer-left' ) ) : ?>
				<div id="footer-left" class="footer-left widget-area" role="complementary">
					<?php dynamic_sidebar( 'footer-left' ); ?>
				</div><!-- #footer-left -->
				<?php endif; ?>

				<?php if ( is_active_sidebar( 'footer-right' ) ) : ?>
				<div id="footer-right" class="footer-right widget-area" role="complementary">
					<?php dynamic_sidebar( 'footer-right' ); ?>
				</div><!-- #footer-left -->
				<?php endif; ?>
			</div>
		</div>

		<div class="site-info">
			<div class="container">
				&#169; <?php echo date("Y"); ?> | Ezekiel J. Emanuel 
				<?php ez_display_copyright_text(); ?>
				<?php ez_display_social_network_links(); ?>
			</div>
		</div><!-- .site-info -->
	</footer><!-- .site-footer container-->

	<?php wp_footer(); ?>

	<?php  ez_display_mobile_menu(); ?>

	</body>
</html>
