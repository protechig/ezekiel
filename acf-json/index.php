<?php
/**
 * The sidebar containing the main widget area.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Ezekiel
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
