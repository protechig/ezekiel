<?php
/**
 * Template Name: Books
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ezekiel
 */

get_header(); 
?>

<div class="display-flex grid-wrapper">
	<header class="entry-header content-header">
		<?php the_title( '<h1 class="entry-title container">', '</h1>'); ?>
	</header>

	<main id="main" class="site-main">
		<article class="books hentry">
			<div class="book-cover third">
				<?php if( get_field('book_cover') ): ?>
				<a href="<?php the_field('book_link'); ?>" target="_blank" class="book-cover-image">
					<img src="<?php the_field('book_cover'); ?>" alt="">
				</a>
				<?php else: ?>
				<a href="<?php the_field('book_link'); ?>" target="_blank" class="book-cover-image">
					<div class="book-cover book-cover-placeholder">
						<span class="book-cover-placeholder-text">Book Cover Unavailable</span>
					</div>
				</a>
				<?php endif; ?>

				<a href="<?php the_field('book_link'); ?>" class="book-link" target="_blank">purchase book »</a>
			</div>

            <div class="book-excerpt right-two-thirds"><?php the_field('book_excerpt'); ?></div>
		</article>
	</main>
</div>

<?php get_footer(); ?>
