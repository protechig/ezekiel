<?php

function customizer_settings( $wp_customize ) {
    $wp_customize->add_setting(
        'site_title_bold',
        array(
            'default' => '',
            'type' => 'theme_mod',
            'capability' => 'edit_theme_options'
        ),
    );

    $wp_customize->add_control( new WP_Customize_Control(
        $wp_customize,
        'site_title_bold',
        array(
            'label'      => __( 'Site Title Bold', 'textdomain' ),
            'settings'   => 'site_title_bold',
            'priority'   => 9,
            'section'    => 'title_tagline',
            'type'       => 'text',
        )
    ) );

    $wp_customize->add_section( 'blogname' , array(
        'label'      => __( 'Site Title Light', 'textdomain' ),
        'title'		=> __( 'Section Title Light', 'ezekiel' ),
        'priority'	=> 20,
    ));
}
add_action( 'customize_register', 'customizer_settings' );
