<?php
/**
 * Template Name: External Article
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ezekiel
 */

get_header();
?>

<div class="display-flex grid-wrapper">
	<header class="entry-header content-header">
		<?php the_title( '<h1 class="entry-title external-article-title">', '</h1>'); ?>
	</header>

	<main id="main" class="site-main">
		<article class="page hentry">
			<span class="external-article-date"><?php the_field('external_article_date'); ?></span>

			<p class="external-article-meta">
				<span class="external-article-author">Written by <?php the_field('external_article_author'); ?> for </span>
				<a href="<?php the_field('external_article_link'); ?>" class="external-article-link" target="_blank"><span class="external-article-outlet"><?php the_field('external_article_outlet'); ?></span> –– Read Now</a>
			</p>
		</article>
	</main>
</div>

<?php get_footer(); ?> 
