<?php
/**
 * Template Name: Sidebar Left
 *
 * This template displays a page with a sidebar on the right side of the screen.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ezekiel
 */

get_header(); 
?>

	<div class="display-flex grid-wrapper">
		<?php 
		while ( have_posts() ) :
			the_post();
		
			get_template_part( 'template-parts/content-header', 'page' );
		
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
		
		endwhile; // End of the loop.
		?>

		<div class="sidebar">
		<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
			<div id="sidebar-1" class="widget-area" role="complementary">
				<?php dynamic_sidebar( 'sidebar-1' ); ?>
			</div>
		<?php endif; ?>

		<div id="sidebar-2" class="sidebar-menu" role="complementary">
			<div class="menu">
				<?php the_field('content'); ?>
			</div>
		</div>
		</div>

		<main id="main" class="site-main">
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</div><!-- .display-flex -->

<?php get_footer(); ?>
 