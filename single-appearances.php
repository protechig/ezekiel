<?php
/**
 * Template Name: Appearance
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ezekiel
 */

get_header(); 
?>

<div class="display-flex grid-wrapper">
	<header class="entry-header content-header">
		<h1 class="entry-title container"><?php the_field('appearance_date'); ?></h1>
	</header>

	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
		<aside id="sidebar-1" class="sidebar widget-area" role="complementary">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
		</aside>
	<?php endif; ?>

	<main id="main" class="site-main">
		<article class="page hentry">
			<a href="<?php the_field('appearance_link'); ?>" class="entry-title" target="_blank">
				<?php the_title( '<h1 class="entry-title">', '</h1>'); ?>
			</a>

			<p>
			Watch the segment <a href="<?php the_field('appearance_link'); ?>" target="_blank">here</a>. <?php the_field('appearance_date'); ?>. <?php the_field('news_outlet'); ?>
			</p>
		</article>
	</main>
</div>

<?php get_footer(); ?>
