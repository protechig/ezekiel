<?php
/**
 * Template Name: Appearances Archive
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ezekiel
 */

get_header(); 
?>

<div id="appearances-archive" class="display-flex grid-wrapper container appearances-archive">
	<header class="entry-header content-header">
		<h1 class="entry-title"><?php echo post_type_archive_title(); ?></h1>
	</header>

	<main id="main" class="site-main">
		<?php if ( have_posts() ) : ?>
			<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					/*
						* Include the Post-Format-specific template for the content.
						* If you want to override this in a child theme, then include a file
						* called content-___.php (where ___ is the Post Format name) and that will be used instead.
						*/
					get_template_part( 'template-parts/content-appearances' );

				endwhile;

				ez_display_numeric_pagination();

			else :

				get_template_part( 'template-parts/content-appearances');

			endif;
			?>

	</main>
</div>

<?php get_footer(); ?>
