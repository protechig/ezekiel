<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Ezekiel
 */

get_header(); ?>

	<div class="display-flex grid-wrapper">
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', get_post_format() );

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- .grid-wrapper -->
<?php get_footer(); ?>
