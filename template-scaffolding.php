<?php
/**
 * Template Name: Scaffolding
 *
 * Template Post Type: page, scaffolding, ez_scaffolding
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ezekiel
 */

get_header(); ?>

	<main id="main" class="site-main container">

		<?php do_action( 'ez_scaffolding_content' ); ?>

	</main><!-- #main -->

<?php get_footer(); ?>
