<?php
/**
 * Template Name: External Articles Archive
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ezekiel
 */

get_header(); 
?>

<div class="display-flex grid-wrapper container">
	<div id="external-articles-archive" class="external-articles-archive">
		<header class="entry-header content-header">
			<h1 class="entry-title container">Academic & Popular Writing</h1>
		</header>

		<main id="main" class="site-main">
			<?php if ( have_posts() ) : ?>
				<?php
					/* Start the Loop */
					while ( have_posts() ) :
						the_post();

						/*
						* Include the Post-Format-specific template for the content.
						* If you want to override this in a child theme, then include a file
						* called content-___.php (where ___ is the Post Format name) and that will be used instead.
						*/
						get_template_part( 'template-parts/content-external-articles' );

					endwhile;

					ez_display_numeric_pagination();

				else :

					get_template_part( 'template-parts/content-none');

				endif;
				?>
		</main>
	</div>
</div>

<?php get_footer(); ?>
