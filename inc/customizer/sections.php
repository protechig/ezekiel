<?php
/**
 * Customizer sections.
 *
 * @package Ezekiel
 */

/**
 * Register the section sections.
 *
 * @author WDS
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function ez_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'ez_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'ezekiel' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'ez_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'ezekiel' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'ezekiel' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'ez_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'ezekiel' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'ez_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'ezekiel' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'ez_customize_sections' );
